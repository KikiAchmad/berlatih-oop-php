<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Berlatih OOP PHP</title>
</head>
<body>
    
    <h3>Release 0</h3>
    <?php

    require_once('animal.php');
    require_once('ape.php');
    require_once('frog.php');

    $sheep = new Animal("shaun");

    echo " Nama binatang : $sheep->name";
    echo "<br>";
    echo " Jumlah kaki : $sheep->legs";
    echo "<br>";
    echo " Berdarah dingin : $sheep->cold_blooded";
    echo "<br>";

    echo "<h3>Release 1</h3>";

    $sungokong = new Ape("kera sakti");
    echo " Nama binatang : $sungokong->name";
    echo "<br>";
    echo " Jumlah kaki : $sungokong->legs";
    echo "<br>";
    echo " Berdarah dingin : $sungokong->cold_blooded";
    echo "<br>";
    $sungokong->yell();
    echo "<br><br>";

    $kodok = new Frog("buduk");
    echo " Nama binatang : $kodok->name";
    echo "<br>";
    echo " Jumlah kaki : $kodok->legs";
    echo "<br>";
    echo " Berdarah dingin : $kodok->cold_blooded";
    echo "<br>";
    $kodok->jump();

    ?>
    
</body>
</html>